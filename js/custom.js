
/*__________ WOW JS +++ __________*/
try {
	new WOW().init();
}
catch(e) {
	console.warn("WOW JS cannot find elements");
}
/*__________ WOW JS --- __________*/





/*__________ Language +++ __________*/
$(".hdr_lang_sel_ttl").click(function () {
	$(this).parent().toggleClass("active");
});

$(document).click(function(e) {
	if ($(e.target).is(".hdr_lang_sel, .hdr_lang_sel *") === false) {
		$(".hdr_lang_sel").removeClass("active");
	}
});

$(".hdr_lang_sel_ls > li").click(function () {
	$(".hdr_lang_sel_ttl").html($(this).html());

	$(".hdr_lang_sel").removeClass("active");
});
/*__________ Language --- __________*/




/*__________ Baner Top +++ __________*/
$(document).ready(function() {

	try {
		var owl = $(".hdr_bnr");

		owl.owlCarousel({
			// autoPlay : 5000,
			stopOnHover : true,
			slideSpeed : 400,
			singleItem: true,
			navigation: false,
			pagination: false,
			// transitionStyle: "goDown",
		});


		// Custom Navigation Events
		$(".hdr_bnr_btn.next").click(function(){
			owl.trigger('owl.next');
		});
		$(".hdr_bnr_btn.prev").click(function(){
			owl.trigger('owl.prev');
		});
	}
	catch(e) {
		console.warn("'hdr_bnr' Carousel cannot find elements");
	}

});
/*__________ Baner Top --- __________*/





/*__________ Partners +++ __________*/
$(document).ready(function() {

	try {
		var owl = $(".part_bnr");

		owl.owlCarousel({
			items: 5,
			lazyLoad: true,
			navigation: false,
			pagination: false
		});


		// Custom Navigation Events
		$(".part_bnr_btn.next").click(function(){
			owl.trigger('owl.next');
		});
		$(".part_bnr_btn.prev").click(function(){
			owl.trigger('owl.prev');
		});
	}
	catch(e) {
		console.warn("'part_bnr' Carousel cannot find elements");
	}

});
/*__________ Partners --- __________*/





/*__________ Header Down Button +++ __________*/
$(".hdr_bnr_ft_btn").click(function () {
	var scrollFromTop = $(".how_play_sec").offset().top;

	$('html, body').animate({ scrollTop: scrollFromTop }, 1000);
});
/*__________ Header Down Button --- __________*/






/*__________ Anchor +++ __________*/
$(".ftr_btn_anchor").click(function () {
	$("html, body").animate({ scrollTop: 0 }, 1000);
});
/*__________ Anchor --- __________*/






/*__________ Fixed Header +++ __________*/
var windowHeight = window.innerHeight;

$(window).on('scroll', function () {
	if($(window).scrollTop() >= windowHeight)
	{
		$(".hdr_top_sec").addClass('fixed');
	}
	else if($(window).scrollTop() == 0)
	{
		$(".hdr_top_sec").removeClass('fixed');
	}
});



$(window).on('scroll', function () {
	if($(window).scrollTop() >= 200)
	{
		$(".hdr_nav_bx").addClass('fixed');
	}
	else
	{
		$(".hdr_nav_bx").removeClass('fixed');
	}
});
/*__________ Fixed Header --- __________*/





/*__________ User Menu +++ __________*/
$(document).on("click", ".hdr_nav_us_head > a", function () {
	$(this).closest(".hdr_nav_us").toggleClass("active");

	return false;
});

$(document).click(function(e) {
	if ($(e.target).is('.hdr_nav_us, .hdr_nav_us *') === false) {
		$('.hdr_nav_us').removeClass('active');
	}
});
/*__________ User Menu --- __________*/





/*__________ Mobile Menu +++ __________*/
$(document).on('click', '.hdr_nav_menu_btn, .hdr_nav_menu_bg', function () {
	$('.hdr_nav_menu_btn').toggleClass('active');
	$('.hdr_nav_menu').toggleClass('active');
	$('.hdr_nav_menu_bg').toggleClass('active');
	$('html').toggleClass('ov-h');
});
/*__________ Mobile Menu --- __________*/






/*__________ Result Tab +++ __________*/
$('.tm_res_tab_nav > li:not(.link) > a').click(function (e) {
	e.preventDefault();
	$(this).tab('show');
});
/*__________ Result Tab --- __________*/





/*__________ Switch +++ __________*/
$(document).on('click', '.tm_ctrl_swch', function () {
	$(this).children('.bar').toggleClass('active');
});
/*__________ Switch --- __________*/






/*__________ Malihu Scrollbar +++ __________*/
$(window).on("load resize", function () {
	
	try {
		var windowWidth = window.innerWidth;
		
		if (windowWidth < 768)
		{
			$(".stats_tb_asd").mCustomScrollbar("destroy");
			
			$(".stats_tb_bx").mCustomScrollbar({
				axis:"x",
				advanced:{autoExpandHorizontalScroll:true},
				scrollInertia: 200
			});
		}
		else {
			$(".stats_tb_asd").mCustomScrollbar({
				axis:"x",
				advanced:{autoExpandHorizontalScroll:true},
				scrollInertia: 200
			});
			
			$(".stats_tb_bx").mCustomScrollbar("destroy");
		}
	}
	catch(e) {
		console.warn("MalihuScrollBar cannot find elements");
	}
	
});
/*__________ Malihu Scrollbar --- __________*/







/*__________ Ligue Top +++ __________*/
$(document).ready(function() {
	
	try {
		var owl = $(".lig_bnr");
		
		owl.owlCarousel({
			singleItem: true,
			navigation: false,
			pagination: false
		});
		
		
		// Custom Navigation Events
		$(".lig_bnr_btn.next").click(function(){
			owl.trigger('owl.next');
		});
		$(".lig_bnr_btn.prev").click(function(){
			owl.trigger('owl.prev');
		});
	}
	catch(e) {
		console.warn("'lig_bnr' Carousel cannot find elements");
	}
	
});
/*__________ Ligue Top --- __________*/







/*__________ Result Tab +++ __________*/
$('.trsf_mdl_his_btn_ls > li:not(.link) > a').click(function (e) {
	e.preventDefault();
	$(this).tab('show');
});
/*__________ Result Tab --- __________*/






/*__________ International Tel Input +++ __________*/
try {
	$(".intl_tel").intlTelInput({
		// allowDropdown: false,
		// autoHideDialCode: false,
		// autoPlaceholder: "off",
		// dropdownContainer: "body",
		// excludeCountries: ["us"],
		// formatOnDisplay: false,
		geoIpLookup: function(callback) {
			$.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
				var countryCode = (resp && resp.country) ? resp.country : "";
				callback(countryCode);
			});
		},
		// initialCountry: "auto",
		// nationalMode: false,
		onlyCountries: ['uz', 'ru', 'en', 'kz', 'kg', 'kr'],
		// placeholderNumberType: "MOBILE",
		preferredCountries: ['uz'],
		// separateDialCode: true,
		// utilsScript: "lib/int_tel_input/js/utils.js"
	});
}
catch(e) {
	console.warn("International Tel Input cannot find elements");
}
/*__________ International Tel Input --- __________*/






/*__________ Statistics Number +++ __________*/
var check = true;
$(window).on('load scroll', function()
{
	try {

		if($(window).scrollTop() + $(window).height() >= $('.site_sts_num').offset().top)
		{
			if (check) {
				check = false;

				$('.site_sts_num').each(function () {
					$(this).prop('Counter',0).animate({
						Counter: $(this).text()
					}, {
						duration: 4000,
						easing: 'swing',
						step: function (now) {
							$(this).text(Math.ceil(now));
						}
					});
				});
			}
		}
	}
	catch(e) {
		console.warn("Cannot find elements '.site_sts_num'");
	}
});
/*__________ Statistics Number --- __________*/
























/*__________ Validation +++ __________*/

try {

}
catch(e) {
	console.warn("Something cannot find elements");
}
